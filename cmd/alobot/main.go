package main

import (
	"context"
	"log"

	tele "ghn.vn/gstation/alobot/tele"
)

func main() {
	api := tele.NewApi("415042532:AAHen7B7NEhMHi2ZJIsbnHY_VlP2w4Xz0pQ")
	bot := tele.NewTeleBotWithAPI(api)
	netCtx, cancel := context.WithCancel(context.Background())
	defer cancel()
	if user, err := api.GetMe(netCtx); err != nil {
		log.Panic(err)
	} else {
		log.Printf("bot info: %#v", user)
	}
	err := bot.Serve(netCtx)
	if err != nil {
		log.Fatal(err)
	}
}
