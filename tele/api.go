package tele

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const (
	APIEndpoint            = "https://api.telegram.org/bot%s/%s"
	errRequestCanceled     = "net/http: request canceled"
	errRequestCanceledConn = "net/http: request canceled while waiting for connection"
)

type DefaultCfg struct{}

func (cfg DefaultCfg) Name() string {
	return getMeMethod
}

// Values for getMe is empty
func (cfg DefaultCfg) Values() (url.Values, error) {
	return nil, nil
}

type API struct {
	token       string
	apiEndpoint string
	client      HTTPTest
}

type HTTPTest interface {
	Do(*http.Request) (*http.Response, error)
}

func NewApi(token string) *API {
	return NewClientWithAPI(token, http.DefaultClient)
}

func NewClientWithAPI(tok string, client HTTPTest) *API {
	return &API{
		token:       tok,
		apiEndpoint: APIEndpoint,
		client:      client,
	}
}

//-----------------------------------------------------
func (c *API) Invoke(ctx context.Context, u UrlTele, l interface{}) error {
	p, err := u.Values()
	if err != nil {
		return err
	}

	req, err := c.getFormRequest(u.Name(), p)
	if err != nil {
		return err
	}
	return c.createRequest(ctx, req, l)
}
func (c *API) getFormRequest(name string, p url.Values) (*http.Request, error) {
	url := fmt.Sprintf(c.apiEndpoint, c.token, name)
	body := p.Encode()
	req, err := http.NewRequest(
		"POST",
		url,
		strings.NewReader(body),
	)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	return req, nil
}

func (c *API) createRequest(ctx context.Context, req *http.Request, l interface{}) error {
	var (
		err  error
		resp *http.Response
	)
	resp, err = createRequest(ctx, c.client, req)
	if err != nil {
		return err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			// todo
		}
	}()
	if resp.StatusCode == http.StatusForbidden {
		if _, err = io.Copy(ioutil.Discard, resp.Body); err != nil {

		}
		return fmt.Errorf("errForbidden")
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	apiResponse := APIResponse{}
	err = json.Unmarshal(data, &apiResponse)
	if err != nil {
		return err
	}
	if !apiResponse.Ok {
		if apiResponse.ErrorCode == 401 {
			return fmt.Errorf("Unauthorized")
		}
		return fmt.Errorf(apiResponse.Description)

	}
	if l != nil && apiResponse.Result != nil {
		err = json.Unmarshal(*apiResponse.Result, l)
	}
	return err
}

//================================
func (c *API) GetUpdates(ctx context.Context, cfg UpdateCfg) ([]Update, error) {
	updates := []Update{}
	if err := c.Invoke(ctx, cfg, &updates); err != nil {
		return nil, err
	}
	return updates, nil
}

func (c *API) GetMe(ctx context.Context) (*User, error) {
	u := &User{}
	if err := c.Invoke(ctx, DefaultCfg{}, u); err != nil {
		return nil, err
	}
	return u, nil
}
