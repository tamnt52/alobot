package tele

import "fmt"

type ValidationError struct {
	Field       string `json:"field"`
	Description string `json:"description"`
}

//====================================
func NewValidationError(field string, description string) *ValidationError {
	return &ValidationError{
		Field:       field,
		Description: description,
	}
}

//=========internal====================
func (e *ValidationError) Error() string {
	return fmt.Sprintf(
		"field %s is invalid: %s",
		e.Field,
		e.Description)
}
