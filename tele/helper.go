package tele

import (
	"golang.org/x/net/context"
)

func GetUpdates(ctx context.Context, api *API, cfg UpdateCfg, out chan<- Update) error {
	var rErr error
	defer close(out)

loop:
	for {
		updates, err := api.GetUpdates(ctx, cfg)
		if err != nil {
			rErr = err
			break loop
		}
		for _, update := range updates {
			if update.UpdateID >= cfg.Offset {
				cfg.Offset = update.UpdateID + 1

				select {
				case <-ctx.Done():
					rErr = ctx.Err()
					break loop
				case out <- update:
				}
			}
		}
	}
	return rErr
}

func NewUpdate(offset int64) UpdateCfg {
	return UpdateCfg{
		Offset:  offset,
		Limit:   0,
		Timeout: 30,
	}
}
