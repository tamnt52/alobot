package tele

import (
	"net/url"
)

type UrlTele interface {
	Name() string
	Values() (url.Values, error)
}
