package tele

import (
	"context"
	"net/http"
	"net/url"
)

func createRequest(ctx context.Context, client HTTPTest, req *http.Request) (*http.Response, error) {
	var (
		resp *http.Response
		err  error
	)
	if httpClient, ok := client.(*http.Client); ok {
		resp, err = httpClient.Do(req.WithContext(ctx))
	} else {
		resp, err = client.Do(req)
	}
	if err != nil {
		if urlErr, casted := err.(*url.Error); casted {
			if urlErr.Err == context.Canceled {
				return resp, context.Canceled
			}
			errMsg := urlErr.Err.Error()
			if errMsg == errRequestCanceled ||
				errMsg == errRequestCanceledConn {
				return resp, context.Canceled
			}
		}
	}
	return resp, err
}
