package tele

import (
	"strings"

	"golang.org/x/net/context"
)

type Handler interface {
	Handle(context.Context) error
}

type Commander interface {
	Command(ctx context.Context, arg string) error
}

type InlineCallback interface {
	Callback(ctx context.Context, data string) error
}
type (
	MiddlewareFunc func(next Handler) Handler
	HandlerFunc    func(context.Context) error
	ErrorFunc      func(ctx context.Context, err error)
	CommandFunc    func(ctx context.Context, arg string) error
	CallbackFunc   func(ctx context.Context, data string) error
)

func (h HandlerFunc) Handle(c context.Context) error {
	return h(c)
}

func (c CommandFunc) Command(ctx context.Context, arg string) error {
	return c(ctx, arg)
}

func (c CallbackFunc) Callback(ctx context.Context, data string) error {
	return c(ctx, data)
}

func Commands(commands map[string]Commander) MiddlewareFunc {
	return func(next Handler) Handler {
		return HandlerFunc(func(ctx context.Context) error {
			update := GetUpdate(ctx)
			if update.Message == nil {
				return next.Handle(ctx)
			}
			command, arg := update.Message.Command()
			if command == "" {
				return next.Handle(ctx)
			}
			cmd, ok := commands[command]
			if !ok {
				if cmd = commands[""]; cmd == nil {
					return next.Handle(ctx)
				}
			}
			if cmd == nil {
				return next.Handle(ctx)
			}
			return cmd.Command(ctx, arg)
		})
	}
}

func Callbacks(callbacks map[string]InlineCallback) MiddlewareFunc {
	return func(next Handler) Handler {
		return HandlerFunc(func(ctx context.Context) error {
			update := GetUpdate(ctx)
			if update.CallbackQuery == nil {
				return next.Handle(ctx)
			}
			queryData := strings.SplitN(update.CallbackQuery.Data, ":", 2)
			var prefix, data string
			switch len(queryData) {
			case 2:
				data = queryData[1]
				fallthrough
			case 1:
				prefix = queryData[0]
			default:
				return next.Handle(ctx)
			}
			callback, ok := callbacks[prefix]
			if !ok {
				if callback = callbacks[""]; callback == nil {
					return next.Handle(ctx)
				}
			}
			if callback == nil {
				return next.Handle(ctx)
			}
			return callback.Callback(ctx, data)
		})
	}
}
