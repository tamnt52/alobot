package tele

import (
	"log"

	"golang.org/x/net/context"
)

type updateKey struct{}
type TeleBot struct {
	api     *API
	handler Handler
	midFunc []MiddlewareFunc
	errFunc ErrorFunc
}

func NewTeleBot(token string) *TeleBot {
	return NewTeleBotWithAPI(NewApi(token))
}

func NewTeleBotWithAPI(api *API) *TeleBot {
	return &TeleBot{
		api:     api,
		midFunc: []MiddlewareFunc{},
		errFunc: func(ctx context.Context, err error) {
			log.Printf("Error: %s", err.Error())
		},
	}
}

func GetUpdate(ctx context.Context) *Update {
	return ctx.Value(updateKey{}).(*Update)
}

//==============================
func (t *TeleBot) TurnOnMidd(m ...MiddlewareFunc) {
	t.midFunc = append(t.midFunc, m...)
}

func (t *TeleBot) Handle(h Handler) {
	t.handler = h
}

func (t *TeleBot) Serve(ctx context.Context) error {
	cfg := NewUpdate(0)
	return t.StartServer(ctx, cfg)
}
func (t *TeleBot) StartServer(ctx context.Context, cfg UpdateCfg) error {
	if err := t.getUpdateMe(ctx); err != nil {
		return err
	}
	var er error
	erChan := make(chan error, 1)
	updateChan := make(chan Update)
	go func() {
		erChan <- GetUpdates(
			ctx,
			t.api,
			cfg,
			updateChan)
	}()
loop:
	for {
		select {
		case er = <-erChan:
			break loop
		case update, ok := <-updateChan:
			if !ok {
				select {
				case er = <-erChan:
					break loop
				}
			}
			t.handleUpdate(ctx, &update)

		}
	}
	return er
}

//============INTERNAL========================
func (t *TeleBot) getUpdateMe(ctx context.Context) error {
	return nil
}

func (t *TeleBot) handleUpdate(ctx context.Context, u *Update) {

}
