package tele

import "encoding/json"

type APIResponse struct {
	Ok          bool             `json:"ok"`
	Result      *json.RawMessage `json:"result"`
	ErrorCode   int              `json:"error_code,omitempty"`
	Description string           `json:"description,omitempty"`
}

type APIError struct {
	Description string `json:"description"`
	ErrorCode   int    `json:"error_code"`
}
