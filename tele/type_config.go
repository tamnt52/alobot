package tele

import (
	"net/url"
	"strconv"
)

type UpdateCfg struct {
	Offset  int64
	Limit   int
	Timeout int
}

func (cfg UpdateCfg) Name() string {
	return getUpdatesMethod
}

func (cfg UpdateCfg) Values() (url.Values, error) {
	if cfg.Limit < 0 || cfg.Limit > 100 {
		return nil, NewValidationError(
			"Limit",
			"should be between 1 and 100",
		)
	}
	v := url.Values{}
	if cfg.Offset != 0 {
		v.Add("offset", strconv.FormatInt(cfg.Offset, 10))
	}
	if cfg.Limit > 0 {
		v.Add("limit", strconv.Itoa(cfg.Limit))
	}
	if cfg.Timeout > 0 {
		v.Add("timeout", strconv.Itoa(cfg.Timeout))
	}
	return v, nil
}
