package tele

type MessageEntity struct {
	Type   string `json:"type"`
	Offset int    `json:"offset"`
	Length int    `json:"length"`
	URL    string `json:"url,omitempty"`
	User   *User  `json:"user,omitempty"`
}

type Audio struct {
	MetaFile
	Duration  int    `json:"duration"`
	Performer string `json:"performer,omitempty"`
	Title     string `json:"title,omitempty"`
	MIMEType  string `json:"mime_type,omitempty"`
}

type Document struct {
	MetaFile
	Thumb    *PhotoSize `json:"thumb,omitempty"`
	FileName string     `json:"file_name,omitempty"`
	MIMEType string     `json:"mime_type,omitempty"`
}

type Sticker struct {
	MetaFile
	Size
	Thumb *PhotoSize `json:"thumb,omitempty"`
	Emoji string     `json:"emoji,omitempty"`
}

// mp4
type Video struct {
	MetaFile
	Size
	Duration int        `json:"duration"`
	MIMEType string     `json:"mime_type,omitempty"`
	Thumb    *PhotoSize `json:"thumb,omitempty"`
}

// Voice object represents a voice note.
type Voice struct {
	MetaFile
	Duration int    `json:"duration"`
	MIMEType string `json:"mime_type,omitempty"`
}

// Contact object represents a phone contact of Telegram user
type Contact struct {
	PhoneNumber string `json:"phone_number"`
	FirstName   string `json:"first_name"`
	UserID      int64  `json:"user_id,omitempty"`
	LastName    string `json:"last_name,omitempty"`
}

type Location struct {
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

type Venue struct {
	Location     Location `json:"location"`
	Title        string   `json:"title"`
	Address      string   `json:"address"`
	FoursquareID string   `json:"foursquare_id,omitempty"`
}

type PhotoSize struct {
	MetaFile
	Size
}
type Size struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

type File struct {
	MetaFile
	FilePath string `json:"file_path,omitempty"`
	Link     string `json:"link"`
}

type MetaFile struct {
	FileID   string `json:"file_id"`
	FileSize int    `json:"file_size,omitempty"`
}
