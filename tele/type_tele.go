package tele

import "strings"

type User struct {
	ID        int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name,omitempty"`
	Username  string `json:"username,omitempty"`
}

type Chat struct {
	ID        int64  `json:"id"`
	Type      string `json:"type"`
	Title     string `json:"title,omitempty"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Username  string `json:"username,omitempty"`
}

type ChatMember struct {
	User   User   `json:"user"`
	Status string `json:"status"`
}
type Message struct {
	MessageID             int64           `json:"message_id"`
	From                  *User           `json:"from,omitempty"`
	Date                  int             `json:"date"`
	Chat                  Chat            `json:"chat"`
	ForwardFrom           *User           `json:"forward_from,omitempty"`
	ForwardFromChat       *Chat           `json:"forward_from_chat,omitempty"`
	ForwardDate           int             `json:"forward_date"`
	ReplyToMessage        *Message        `json:"reply_to_message,omitempty"`
	EditDate              int             `json:"edit_date,omitempty"`
	Entities              []MessageEntity `json:"entities"`
	Text                  string          `json:"text,omitempty"`
	Audio                 *Audio          `json:"audio,omitempty"`
	Document              *Document       `json:"document,omitempty"`
	Photo                 []PhotoSize     `json:"photo,omitempty"`
	Sticker               *Sticker        `json:"sticker,omitempty"`
	Video                 *Video          `json:"video,omitempty"`
	Voice                 *Voice          `json:"voice,omitempty"`
	Caption               string          `json:"caption,omitempty"`
	Contact               *Contact        `json:"contact,omitempty"`
	Location              *Location       `json:"location,omitempty"`
	Venue                 *Venue          `json:"venue,omitempty"`
	NewChatMember         *User           `json:"new_chat_member,omitempty"`
	LeftChatMember        *User           `json:"left_chat_member,omitempty"`
	NewChatTitle          string          `json:"new_chat_title,omitempty"`
	NewChatPhoto          []PhotoSize     `json:"new_chat_photo,omitempty"`
	DeleteChatPhoto       bool            `json:"delete_chat_photo,omitempty"`
	GroupChatCreated      bool            `json:"group_chat_created,omitempty"`
	SuperGroupChatCreated bool            `json:"supergroup_chat_created,omitempty"`
	ChannelChatCreated    bool            `json:"channel_chat_created,omitempty"`
	MigrateToChatID       int64           `json:"migrate_to_chat_id,omitempty"`
	MigrateFromChatID     int64           `json:"migrate_from_chat_id,omitempty"`
	PinnedMessage         *Message        `json:"pinned_message,omitempty"`
}
type Update struct {
	UpdateID           int64               `json:"update_id"`
	Message            *Message            `json:"message,omitempty"`
	EditedMessage      *Message            `json:"edited_message,omitempty"`
	InlineQuery        *InlineQuery        `json:"inline_query,omitempty"`
	ChosenInlineResult *ChosenInlineResult `json:"chosen_inline_result,omitempty"`
	CallbackQuery      *CallbackQuery      `json:"callback_query,omitempty"`
}

func (m *Message) IsCommand() bool {
	return m.Text != "" && m.Text[0] == '/'
}

func (m *Message) Command() (string, string) {
	arg := ""
	if !m.IsCommand() {
		return "", arg
	}
	splits := strings.SplitN(m.Text, " ", 2)
	command := splits[0][1:]
	if len(splits) == 2 {
		arg = splits[1]
	}

	if i := strings.Index(command, "@"); i != -1 {
		command = command[:i]
	}

	return command, arg
}
